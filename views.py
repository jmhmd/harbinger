from sdkapp import app
from harbinger.sdk.data import RadReport
import org.hibernate.criterion.Restrictions as Restrictions
import org.hibernate.criterion.Order as Order
from pprint import pprint

@app.route('/')
def hello_world():
	return render_template( "index.html" )
	
@app.route('/reports/<historyDepth>', defaults={'radiologist': 'all', 'historyDepth': 10})
def reports(radiologist, historyDepth):
	class Report(object):
		pass
	
	reportList = []
	
	reports = (RadReport.createCriteria()
		.add( Restrictions.ne('reportBody', 'Negative.') )
		.addOrder( Order.desc('reportEvent') )
		.createCriteria('examStatus')
			.add( Restrictions.eq('status', 'Final') )
		.setMaxResults(historyDepth))
		
	for report in reports.list():
		newReport = Report()
		newReport.id = report.id()
		newReport.author = report.rad1().name()
		newReport.body = report.reportBody()
		newReport.status = report.examStatus().status()
		newReport.date = report.reportEvent()
		reportList.append( newReport )
	
	return render_template('reports.html', reports = reportList)