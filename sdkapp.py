from flask import Flask, url_for, render_template, flash, request, session, redirect
from harbinger.sdk.data import RadReport
import org.hibernate.criterion.Restrictions as Restrictions
import org.hibernate.criterion.Order as Order
from pprint import pprint
from com.xhaus.jyson import JysonCodec as json

app = Flask(__name__)

# db connection
RadReport.createCriteria()

@app.route('/')
def hello_world():
	return render_template( "index.html" )
	
@app.route('/something/<hello>/<another>')
def something(hello, another):
	print hello
	print another
	return false

@app.route('/reports', defaults={'radiologist': 'all', 'historyDepth': 10})
@app.route('/reports/<int:historyDepth>', defaults={'radiologist': 'all'})
@app.route('/reports/<string:radiologist>', defaults={'historyDepth': 10})
@app.route('/reports/<string:radiologist>/<int:historyDepth>')
def reports(radiologist, historyDepth, returnRawList=0):
	class Report(object):
		pass
	
	reportList = []
	
	if radiologist == 'all':
		reports = (RadReport.createCriteria()
			.add( Restrictions.ne('reportBody', 'Negative.') )
			.addOrder( Order.desc('reportEvent') )
			.createAlias('examStatus','es')
				.add( Restrictions.eq('es.status', 'Final') )
			.setMaxResults(historyDepth))
	else:
		reports = (RadReport.createCriteria()
			.add( Restrictions.ne('reportBody', 'Negative.') )
			.addOrder( Order.desc('reportEvent') )
			.createAlias('examStatus','es')
				.add( Restrictions.eq('es.status', 'Final') )
			.createAlias('rad1','rd')
			.add( Restrictions.eq('rd.name', radiologist))
			.setMaxResults(historyDepth))
	
	if len(reports.list()) < 1:
		flash('No reports found from that radiologist')
		return redirect(url_for('reports'))
		
	for report in reports.list():
		newReport = Report()
		newReport.id = report.id()
		newReport.author = report.rad1().name()
		newReport.body = report.reportBody()
		newReport.status = report.examStatus().status()
		newReport.date = report.reportEvent()
		reportList.append( newReport )
		
	if returnRawList:
		return reportList;
	
	return render_template('reports.html', reports = reportList, radiologist = radiologist, historyDepth = historyDepth)

@app.route('/annotations/get/<type>', methods=['GET','POST'])
@app.route('/annotations/get/', defaults={'type':'terms'}, methods=['GET','POST'])
def annotations(type):
	
	from tools.annotator import Annotator
	
	an = Annotator()
	
	def getTerms(text):
	# Get simple list of terms that matched
		
		an.params['levelMax'] = 0
		
		result = an.getAnnotations(text)
		annoObj = result['annotations']
		
		pprint(annoObj);
		
		terms = []
		for concept in annoObj:
			terms.append(concept.context.term.concept.preferredName)
		
		pprint(terms)
		return terms
		
	def getAnnotations(text):
	# Get simple list of terms that matched
		
		an.params['levelMax'] = 3
		
		result = an.getAnnotations(text)
		annoObj = result['annotations']
		
		contexts = []
		for concept in annoObj:
		
			if concept.context.contextName == 'CLOSURE':
				term = {}
				term['term'] = concept.context.concept.preferredName
				term['from'] = int(concept.context.from)
				term['to'] = int(concept.context.to)
				term['isA'] = concept.concept.preferredName
				term['link'] = concept.context.concept.fullId
				
			else: # others I know of are mgrepContextBean - seems to mean it does not have is_a closure info
				term = {}
				term['term'] = concept.context.term.concept.preferredName
				term['from'] = int(concept.context.from)
				term['to'] = int(concept.context.to)
				term['link'] = concept.context.term.concept.fullId
				
			contexts.append(term)
		
		pprint(contexts)
		return contexts
	
	if type == 'terms':
		
		text = request.values['text']
		
		return json.dumps(getTerms(text))
		
	elif type == 'frequencies':
		
		reportList = reports(request.values['radiologist'], int(request.values['historyDepth']), 1)
		sort = request.values['sort']
		
		# build list of text for efficient concatenation
		textList = []
		
		for report in reportList:
			textList.append(report.body)
		
		annotationText = ''.join(textList)
		
		pprint(annotationText);
		
		# make request for annotations on block of concatenated reports
		terms = getTerms(annotationText)
		
		# build list of term frequencies
		frequencies = {}
		
		for term in terms:
			if frequencies.has_key(term):
				frequencies[term] += 1
			else:
				frequencies[term] = 1
		
		# sort term frequency dict by term frequency, descending
		from operator import itemgetter
		sortedTerms = sorted(frequencies.iteritems(), key=itemgetter(1))
		
		if sort == 'desc':
			sortedTerms.reverse()
		
		return json.dumps(sortedTerms)
		
	elif type == 'annotations':
		text = request.values['text']
		
		return json.dumps(getAnnotations(text))
		
	else:
		abort(404)
		
	
@app.route('/realtime')
def realtime():
	return render_template('realtime.html', cometdScript = getCometdURLScript() )
	
#-----------HELPER FUNCTIONS ------------------

def getCometdLocation():
	from javax.naming import InitialContext
	context = InitialContext()
	url = context.lookup('harbinger-config').getProperty('cometd-amqp.url')
	return url
	
def getCometdURLScript():
	script = '<script language="javascript">'
	script += '(function($) {'
	script += '$.cometURL = "' + getCometdLocation() + '";'
	script += 'if ($.cometURL == "") { $.cometURL = location.protocol + "//" + location.hostname + ":8080/cometd-amqp/cometd"; };'
	script += '})(jQuery);'
	script += '</script>'
	return script
	
app.secret_key = '.\xad\xf7\xf3\xb0$\x95K/\xe1v\x92dHHn6\x17\xbc\xe4\xf54\x1d'
	
if __name__ == '__main__':
	app.run(debug=True)