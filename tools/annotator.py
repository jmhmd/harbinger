import urllib #@UnresolvedImport
import urllib2 #@UnresolvedImport
import xml2obj
import pprint

class Annotator(object):
	'''
	Created on Aug 3, 2010
	Updated on Apr 26, 2011

	@author: Trish Whetzel
	@contact: support@bioontology.org 
	@note: See the Annotator User Guide for the full list of parameters: http://www.bioontology.org/wiki/index.php/Annotator_User_Guide
	@note: Please subscribe to the bioportal-announce mailing list (https://mailman.stanford.edu/mailman/listinfo/bioportal-announce) to stay informed of any changes to the Annotator Web service 
	'''
	#API key, login to BioPortal (http://www.bioontology.org) and go to Account to see your API key 
	API_KEY= '24e050ca-54e0-11e0-9d7b-005056aa3316'
	API_KEY= '5758f84b-562e-46cb-890b-ff787cc52bed'

	# Base URL for service
	annotatorUrl = 'http://rest.bioontology.org/obs/annotator'
	submitUrl = annotatorUrl + '/submit/jhostetter@gmail.com'

	# Text to Annotate 
	textToAnnotate = "Melanoma is a malignant tumor of melanocytes which are found predominantly in skin but also in the bowel and the eye"

	# Structure containing parameters
	params = {
			  'longestOnly':'false',
			  'wholeWordOnly':'true',
			  'withContext':'true',
			  'filterNumber':'true', 
			  'stopWords':'',
			  'withDefaultStopWords':'false', 
			  'isStopWordsCaseSenstive':'false', 
			  'minTermSize':'3', 
			  'scored':'true',  
			  'withSynonyms':'true', 
			  'ontologiesToExpand':'1057',   
			  'ontologiesToKeepInResult':'1057',   
			  'isVirtualOntologyId':'true', 
			  'semanticTypes':'',  #T017,T047,T191&" #T999&"
			  'levelMax':'3',
			  'mappingTypes':'null', 
			  'textToAnnotate':'', 
			  'format':'xml',  #Output formats (one of): xml, tabDelimited, text  
			  'apikey':API_KEY,
	}

	def getAnnotations(self,text):
		result = {}
		
		self.params['textToAnnotate'] = text
		
		# Submit job
		postData = urllib.urlencode(self.params)
		fh = urllib2.urlopen(self.submitUrl, postData)
		annotatorResults = fh.read()
		fh.close()

		# results in xml format, convert to object
		result['rawObj'] = xml2obj.xml2obj(annotatorResults)
		result['annotations'] = result['rawObj']['data'].annotatorResultBean.annotations.annotationBean
		
		# return job identifier
		return result

