##Real Time Data
This is a place holder for what this is all about and why you'd use it and a thousand points of light.

###Prerequisites
Harbingerjs gives you some very handy tools for getting real time data in your web application, but there are prerequisite libraries and steps.

####CometURL

In order to connect to real time data you need to have the url for cometd set in your application and passed to javascript land. The cometd url should be pulled from the jndi. The location is ```harbinger-config``` and the property name is ```cometd-amqp.url```. You can do this in ruby with the following:

```ruby
def get_cometd_location
  begin
    @cometd_url = javax.naming.InitialContext.new.lookup("harbinger-config").getProperty("cometd-amqp.url")
  rescue NativeException => e
    @cometd_url = ""
  end
end
```

Typically I put this method in the ```ApplicationController``` and then add a ```before_filter :get_cometd_location``` to the ```ApplicationController``` as well. This lets you then use the ```@cometd_url``` instance variable in your layout/view so you can pass it to the javascript.

To pass this to the javascript you'll need to have your application write javascript into your view/layout. Here is a rails helpers that does just that as well as give a default location if no jndi is available.

```ruby
module ApplicationHelper
  #...
  def setup_cometd_url(cometd_url)
    '<script language="javascript">
       (function($) {
         $.cometURL = "' + cometd_url.to_s + '";
         if ($.cometURL == "") { $.cometURL = location.protocol + "//" + location.hostname + ":8080/cometd-amqp/cometd"; };
       })(jQuery);
     </script>'
  end
  #...
end
```

Once that helper is defined you can reference it with the rest of the libraries that need to be included.

####Libraries

You'll need to include the following libraries in this order:

1. jquery
2. json2
3. harbingerjs/cometd/cometd.js
4. harbingerjs/cometd/jquery.cometd.js
5. harbingerjs/amqp-listener.js

To do this in a rails application you can use the following snippet:

```erb
<!-- cometd Setup -->
<%= setup_cometd_url(@cometd_url) %> <!-- This was defined in the helper above -->
<%= javascript_include_tag 'harbingerjs/cometd/cometd.js' %>
<%= javascript_include_tag 'harbingerjs/cometd/jquery.cometd.js' %>

<!-- harbingerjs Setup -->
<%= javascript_include_tag 'harbingerjs/core.js' %>
<%= javascript_include_tag 'harbingerjs/data_table.js' %>
<%= javascript_include_tag 'harbingerjs/flot.js' %>
<%= javascript_include_tag 'harbingerjs/amqp-listener.js' %>
```

***

###Connecting
Cometd works by querying the server with a handshake request. This handshake determines the protocol(s) supported by the server and subsequently used by the cometd javascript libraries. After a successful handshake the library attempts to connect. Cometd (the Bayeux protocol specifically) has an architecture for connection and channel management, but harbingerjs abstracts all of that away.  To open a connection to the real time data feeds simply use the following javascript:
```javascript
$(document).ready(function() {
  //Remember! The $.cometURL was set during the prerequisite tasks
  $.harbingerjs.amqp.setup({url: $.cometURL});
});
```
You should now be able to see a cometd connecting to the server with your browser's developer tools.

***

###Binding
*Warning* if you look at cometd documentation you'll likely see similar concepts (binding, channels, etc) to what we are about to talk about, but we will be referring to the harbinger AMQP server, not cometd.

Binding is how you get messages moving across the amqp exchanges sent to your browser. The exchange you will connect to is called ```audit``` and provides the feed of all the database transactions. The routing key format is ```tablename.operation_type.database_row_id``` where tablename is the name of the table in the database, operation type is the type of operation performed (insert,update), and id is the id of the row associated with the message.
The payload consists of the pre and post values for that transaction.

Binding takes 2 arguments. The first is the exchange name and the second is the routing key matcher. If you are unfamiliar with what a routing key matcher is please see the rabbitmq documentation on topic exchanges (http://www.rabbitmq.com/tutorials/tutorial-five-python.html).

To bind to an exchange you use the ```$.harbingerjs.amqp.bind``` command once you've run the setup command. Here is an example of connecting and binding to get all audit messages.

```javascript
$(document).ready(function() {
  $.harbingerjs.amqp.setup({url: $.cometURL});
  $.harbingerjs.amqp.bind("audit","#",
                          function(bindMessage) { ... }, //An optional onBind callback
                          function(unBoundMessage) { ... }); //An optional onUnbound callback
```

As you might have guessed there is an unbind method that takes an exchange, routing key matcher, and a onunbound callback. To unbind an exchange you've bound to you'll need to specify the identical exchange and routing key matcher that you used for binding.

```javascript
$.harbingerjs.amqp.unbind("audit","#",
                          function(unBoundMessage) { /* ... my callback logic ... */ });
```

***

###Callbacks and Listeners

Callbacks are how you tie your javascript functions to the real time data. This could do anything from making an ajax call to redrawing a graph. You can add a callback to the events within the harbingerjs.amqp library with the ```addCallback``` method. It takes the type of event and a function that will be called. The callback types are connect, disconnect, handshake, message, bound, and unbound. Typically you'll only ever use connect, and disconnect directly and other forms of callbacks for message and bindings. Callback and listener methods can be called as many times as you like, just like jQuery's bind method.

#### Disconnect and Connect Callbacks

Here is an example of how and why connect and disconnect can/should be used. All callbacks pass in as the first argument the message associated with the event. It's very possible that cometd can disconnect and it's important to give the user feedback when this happens. Typically it is because the users credentials have expired (they need to log in again). To do this you'll want to add a callback to ```disconnect``` like so:

```javascript
//... Your document.ready etc
  $.harbingerjs.amqp.addCallback('disconnect',function(message) {
    // You can run this as many times as you want to add a callback for when/if
    // a disconnect happens (i.e., you are no longer getting messages
    // I tend to tie this in to a user alert that instructs them to reload the page
    // if this persists and have a callback on connect (see below) that removes
    // that user message
    console.log("Oh crap! Real time data is off!");
  });
//...
```
Giving your user feedback about connection status can be handy as well, especially in conjunction with ```disconnect```. To do this simply add a connect callback:

```javascript
  $.harbingerjs.amqp.addCallback('connect',function(message) {
    // Here is where you can put callbacks around connecting
    // REMEMBER! Being connected and being bound are not the same thing!
    console.log("Connected! Yay!");
  });
```

####Message Callback (Listener)

The ```message``` event type is where your application really uses the real time messages. But much of the time you will want to use the routing key/exchange of the message to determine what callback should run. So that you don't have to write this functionality there is a message specific call back method called ```addListener```. ```addListener``` takes the callback function (which is passed the message object) and an optional routing key matcher and exchange.  This way you can bind to all the messages you want and then build callbacks that focus on specific message types.  Here's a practical example of how to build a listener.

```javascript
// Here is a callback that will be run any time you receive a message
$.harbingerjs.amqp.addListener(function(message) { /* ... */ });

// Here is a callback that will fire only when the routing key matcher matches the
// routing key of the incoming message.
$.harbingerjs.amqp.addListener(function(message) { /* ... */ },"my.routing_key.*.matcher");

// Here is a callback that will fire only when the message comes from the audit exchange
$.harbingerjs.amqp.addListener(function(message) { /* ... */ },"#","audit");
```

Just like with the ```addCallback``` method, ```addListener``` can be called multiple times and all of the given callbacks will be run when they meet the conditions specified by you.

####Bind and Unbind
Sometimes your application is driven entirely by real time messages and you want to be sure that you are getting the messages you expect before you start doing other things. In these cases ```connect``` is not the ideal event to use to determine that you're getting messages. Instead you want to use the ```bind``` and ```unbind``` events.  You can do this in two ways. The first and likely less used, is to use ```addCallback``` to tie a function to a successful bind/unbind. To do this you need to pass a binding object and the callback function to the ```addCallback``` method like so:

```javascript
var bindingObject = {'exchange':'audit','routing_key':'#', 'fun': function(message) { /* ... my callback ... */ }};
$.harbingerjs.amqp.addCallback('bind',bindingObject);

// The same can be done with the 'unbind' event type
$.harbingerjs.amqp.addCallback('unbind',bindingObject);
```

What is more likely is that you'll just add a bind and unbind callback at the time you run the ```bind``` method.

```javascript
$.harbingerjs.amqp.bind("my_exchange","my.routing.key.matcher",
                        function(bindMessage) { ... }, //An optional onBind callback
                        function(unBoundMessage) { ... }); //An optional onUnbound callback

// As you might have guessed there is an unbind method that has a similar structure
// the difference being that this method does not take a binding callback as you are
// intentionally sending the unbind message to the server.
$.harbingerjs.amqp.unbind("my_exchange","my.routing.key.matcher",
                          function(unBoundMessage) { ... }); //An optional onUnbound callback

```

***

###Everything Together
```javascript
$(document).ready(function() {
  $.harbingerjs.amqp.addCallback('disconnect',function(message) {
    // You can run this as many times as you want to add a callback for when/if
    // a disconnect happens (i.e., you are no longer getting messages
    // I tend to tie this in to a user alert that instructs them to reload the page
    // if this persists and have a callback on connect (see below) that removes
    // that user message
    console.log("Oh crap! Real time data is off!");
  });

  $.harbingerjs.amqp.addCallback('connect',function(message) {
    // Here is where you can put callbacks around connecting
    // REMEMBER! Being connected and being bound are not the same thing!
    console.log("Connected! Yay!");
  });

  // Setting things up requires you to specify the url 
  // This is where you reference the javascript variable you made in your application
  // controller after pulling the value from the servers jndi. You could do this in an ajax
  // call, but why bother when you know on the page query what answer is.
  $.harbingerjs.amqp.setup({url: $.cometURL});

  //To act on the messages you get you need to add listeners with
  $.harbingerjs.amqp.addListener(function(message) {}, optional_routing_key_matcher, optional_exchange);
  // The message sent to your callback function is an object with the exchange,
  // the routing key, and the payload. If the payload is JSON then it is parsed
  // automagically.
  // If you don't specify a routing key matcher or exchange then it will fire that
  // callback no matter matter what exchange and routing key that message has.

  // Here comes where the magic happens, BINDING!
  // Binding is how you build a queue, tie it to an exchange, and subscribe to it.
  // That process is taken care of you by our amqp-cometd-bridge application. So all
  // have to worry about is the javascript.
  // The bind method takes an exchange, a routing key matcher, and two optional callbacks
  // The first is the onbind callback. This is called when a bound message is received
  // telling you that the bind you requests has been done and you should start getting
  // messages for it now.
  // The second is the onunbind callback which fires when a binding has been unbound
  $.harbingerjs.amqp.bind("my_exchange","my.routing.key.matcher",
                          function(bindMessage) { ... }, //An optional onBind callback
                          function(unBoundMessage) { ... }); //An optional onUnbound callback

  // As you might have guessed there is an unbind method that takes an exchange,
  // routing key, and a onunbound callback. ($.harbingerjs.amqp.unbind(...))
});
```