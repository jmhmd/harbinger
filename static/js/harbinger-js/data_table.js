/*
=Harbinger JS: data=

A library for storing data in table structures and updating them with real time and sentinal events

*/

(function($) {
    if ($.harbingerjs == undefined) { $.harbingerjs = {} }

    $.harbingerjs.dataTable = function(key_id,fields,extensions) {
	if (extensions == undefined) { extensions = [] };
	var retobj = {
	    create: function(data) {
		if ($.type(data) == "array" && data.length == 0) {
		    var hashed_data = {};
		} else if ($.type(data) == "array" && data.length > 0) {
		    var hashed_data = {};
		    $.each(data, function(index,value) { hashed_data[value[key_id]] = $.harbingerjs.typeCastObject(fields,value) });
		} else if ($.type(data) == "object") {
		    var hashed_data = data;
		} else { throw "Unable to build table: data given must be an array or object" }
		var table_scope = this;
		var instanceobj = {
		    raw_data: data,
		    data: hashed_data,
		    key_id: key_id,
		    fields: fields,
		    event_listeners: [],
		    listenWith: function(fun) { this.event_listeners = this.event_listeners.push(fun) },
		    update: function(row) {
			this.data[row[this.key_id]] = $.harbingerjs.typeCastObject(this.fields,row);
			this.sendUpdateEvent(row);
			return this.data;
		    },
		    sendUpdateEvent: function() { $.each(this.event_listeners,function(index,listener) { listener.apply(this,arguments) }) },

		    //Manipulate data and return new data table
		    map: function(fun) {
			var list = this.transform([],function(acc,value) { acc.push(fun(value)); });
			return table_scope.create(list);
		    },

		    clone: function() {
			return table_scope.create($.map(this.data,function(obj) { return $.extend({},obj); }));
		    },

		    rawClone: function() {
			return table_scope.create($.map(this.raw_data,function(obj) { return $.extend({},obj); }));
		    },

		    //iterate over the data, the given functions return is the new accum
		    transform: function(initial,fun) {
			var accumulator = initial;
			$.each(this.data,function(key,value) { accumulator = fun(accumulator,value); });
			return accumulator;
		    },
		    groupWith: function(fun_or_attr) {
			var fun = null;
			if ($.type(fun_or_attr) != "function") { fun = function(row) { return row[fun_or_attr]; } } else { fun = fun_or_attr; }
			return this.transform({},function(accum,row) {
			    var group = fun(row);
			    if (accum[group] == undefined) { accum[group] = []; }
			    accum[group].push(row);
			    return accum;
			});
		    },
		    extreme: function(fun_or_attr,test) {
			var fun = null;
			if ($.type(fun_or_attr) == "function") { fun = fun_or_attr; } else { fun = function(row) { return row[fun_or_attr]; } }
			var extreme = null;
			$.each(this.data,function(key,row) {
			    if (extreme == null) { extreme = fun(row) }
			    else if (test(extreme,fun(row))) { extreme = fun(row) }
			});
			return extreme;
		    },
		    min: function(fun_or_attr) { return this.extreme(fun_or_attr,function(min,current) { return min > current;}) },
		    max: function(fun_or_attr) { return this.extreme(fun_or_attr,function(max,current) { return max < current;}) }
		}
		$.each(extensions,function(index,extension) { $.extend(instanceobj,extension) });
		return instanceobj;
	    }
	}
	return retobj;
    }
})(jQuery);

(function($) {
    if ($.harbingerjs == undefined) { $.harbingerjs = {} }

    $.harbingerjs.parseDate = function(String) {
	var matcher = /(\d{2,4}[-\/]\d{1,2}[-\/]\d{1,2}[T ]\d{1,2}:\d{1,2}:\d{1,2})([- +])(\d{1,2}):(\d{1,2})/i;
	var parts = null;
	if ($.type(String) == "string" && $.type((parts = String.match(matcher))) == "array") {
	    return Date.parse(parts[1]);
	}
	else if ($.type(String) == "date") {
	    return String;
	}
	else {
	    if (Date.parse(String) != null) { return Date.parse(String) }
	    else { throw "Invalid date given to $.harbingerjs.parseDate" }
	}
    }

    $.harbingerjs.typeCastObject = function(fields,hash) {
	var cast_hash = {};
	$.each(hash, function(key,value) {
	    cast_hash[key] = $.harbingerjs.typeCast(fields[key],value);
	});
	return cast_hash;
    };

    $.harbingerjs.typeCast = function(typehint,value) {
	var matcher = /array_of_(.*)/i;
	var parts = null;
	if ($.type(typehint) == "string" && $.type((parts = typehint.match(matcher))) == "array") {
	    return $.map(value, function(i) { return $.harbingerjs.typeCast(parts[1],i); });
	}
	else if (typehint == "date") {
	    var date = null;
	    try { date = $.harbingerjs.parseDate(value) }
	    catch(error) { return value }
	    date.toUTC = function() { return this.getTime() - this.getTimezoneOffset()*60*1000 };
	    return date;
	}
	else if (typehint == "number") {
	    return Number(value);
	}
	else { return value; }
    }
})(jQuery);
