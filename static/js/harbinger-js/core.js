/*
=Harbingerjs Core=

All the general functions used by the harbingerjs library

*/

(function($) {
    if ( typeof harbingerjsRelativeRoot === 'undefined') { var relativeRoot = "/"; } else { var relativeRoot = harbingerjsRelativeRoot }
    if ($.harbingerjs == undefined) { $.harbingerjs = {} }

    $.harbingerjs.core = {};
    var core = $.harbingerjs.core;

    $.extend(core,{
	url: function(address) {
	    var u = relativeRoot + address;
	    return u.replace("//","/");
	}
    });
})(jQuery);
